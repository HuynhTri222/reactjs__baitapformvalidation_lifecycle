import React, { Component } from "react";
import {connect} from "react-redux";
import UserItem from "./userItem";

class ListUser extends Component {
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>MaNV</th>
            <th>TenNV</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          {this.props.listNV.map((user, index) => {
            return <UserItem user={user} key={index}/>;
          })}
        </tbody>
      </table>
    );
  }
}

const mapStateToProps = (state) => {
  return ({
    listNV: state.formValidationReducer.listUser,
  });
}

export default connect(mapStateToProps)(ListUser);
