import React, { Component } from "react";
import ListUser from "./ListUser";
import {connect} from "react-redux";

class FormValidation extends Component {
    state = {
        user: {
            manv: "",
            tennv: "",
            email: ""
        },
        errors: {
            manv: "",
            tennv: "",
            email: ""
        },
        //manvValid: false,
        //tennvValid: false,
        //emailValid: false,
        formValid: false
    };
    handleChange = (e) => {
        this.setState({
            user: {...this.state.user, [e.target.name]: e.target.value}
        }, () => {
            console.log(this.state);
        });
    };

    handleOnBlur = (e) => {
        const { name, value } = e.target;
        let mess = "";
        if(value.trim() === "") { //trim(): bo khoang trong dau cuoi
            mess = name +  " khong duoc rong";
        }
        else if(name === "email" && !value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)){
          mess = name +  " khong dung dinh dang";
        }
        // let { manvValid, tennvValid, emailValid } = this.state;
        // switch (name) {
        //   case "manv":
        //     if (value.length < 4) mess = "Ky tu phai > 3";
        //     manvValid = mess === "" ? true : false;
        //     break;
        //   case "tennv":
        //     tennvValid = mess === "" ? true : false;
        //     break;
        //   case "email":
        //     emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) ? true : false;
        //     break;
        //   default:
        //     break;
        // }
        // this.setState({
        //     errors: {...this.state.errors, [name]: mess},
        //     manvValid,
        //     tennvValid,
        //     emailValid
        // }, () => {
        //     this.validationForm();
        // });
        this.setState(
          {
            errors: { ...this.state.errors, [name]: mess }
          },
          () => {
            const { manv, tennv, email } = this.state.user;
            if (manv === "" || tennv === "" || email === "" || this.state.errors.email !== "") {
              this.setState({
                formValid: false,
              });
            } else {
              this.setState({
                formValid: true,
              });
            }
          }
        );
    };

    validationForm = () => {
        const { manvValid, tennvValid, emailValid } = this.state;
        this.setState({
            formValid: manvValid && tennvValid && emailValid
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.dispatch({
          type: "FORM_SUBMIT",
          payload: this.state.user,
        });
        this.setState({
          user: {
            manv: "",
            tennv: "",
            email: ""
        },
        });
    }

    static getDerivedStateFromProps(newProps, currentState) {
      if(newProps.userEdit && newProps.userEdit.manv !== currentState.user.manv) {
        let newState = {
          ...currentState,
          user: newProps.userEdit
        };
        return newState;
      }
      return null;
    }

  render() {
    return (
      <div className="container">
        <h3 className="title">*FormValidation</h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Ma nhan vien</label>
            {/* onBlur la su kien nguoi dung di chuyen con tro chuot tu o input ra ngoai */}
            <input 
              type="text" 
              className="form-control" 
              name="manv"
              value={this.state.user.manv}
              onChange={this.handleChange} 
              onBlur={this.handleOnBlur}
              disabled={this.props.userEdit}/>
            {this.state.errors.manv !== "" && <div className="alert alert-danger" role="alert">
                <strong>{this.state.errors.manv}</strong>
            </div>}
          </div>
          <div className="form-group">
            <label>Ten nhan vien</label>
            <input 
              type="text" 
              className="form-control" 
              name="tennv" 
              value={this.state.user.tennv}
              onChange={this.handleChange} 
              onBlur={this.handleOnBlur}/>
            {this.state.errors.tennv !== "" && <div className="alert alert-danger" role="alert">
                <strong>{this.state.errors.tennv}</strong>
            </div>}
          </div>
          <div className="form-group">
            <label>Email</label>
            <input 
              type="email" 
              className="form-control" 
              name="email" 
              value={this.state.user.email}
              onChange={this.handleChange} 
              onBlur={this.handleOnBlur}/>
            {this.state.errors.email !== "" && <div className="alert alert-danger" role="alert">
                <strong>{this.state.errors.email}</strong>
            </div>}
          </div>
          <button type="submit" className="btn btn-success" disabled={!this.state.formValid}>
            Submit
          </button>
        </form>
        <ListUser/>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userEdit: state.formValidationReducer.userEdit
  };
}

export default connect(mapStateToProps)(FormValidation);