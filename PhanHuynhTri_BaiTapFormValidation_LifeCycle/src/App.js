import React from 'react';
import logo from './logo.svg';
import './App.css';
import FormValidation from './Form-Validation';

function App() {
  return (
    <div >
      <FormValidation/>
    </div>
  );
}

export default App;
