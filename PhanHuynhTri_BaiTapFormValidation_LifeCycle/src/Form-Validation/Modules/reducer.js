let initialState = {
    listUser: [
        {
            manv: "1234",
            tennv: "nguyen",
            email: "dpnguyen53@gmail.com",
        },
        {
            manv: "1235",
            tennv: "hieu",
            email: "hieu@gmail.com",
        }
    ],
    userEdit: null
};

const formValidationReducer = (state = initialState, action) => {
    switch (action.type) {
        case "EDIT_USER":
            state.userEdit = action.payload;
            return {...state};
        case "FORM_SUBMIT": {
            let tempListUser = [...state.listUser]; //Vi tinh chat shallow compare cua redux
            const index = state.listUser.findIndex((user) => user.manv === action.payload.manv);
            if(index === -1) tempListUser.push(action.payload);
            else {
                tempListUser[index] = action.payload;
                state.userEdit = null; 
            }
            state.listUser = tempListUser;
            console.log(state);
            return {...state};

        }
    
        default:
            return {...state};

    }

}

export default formValidationReducer;