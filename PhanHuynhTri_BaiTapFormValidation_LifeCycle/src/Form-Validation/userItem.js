import React, { Component } from 'react';
import {connect} from "react-redux";

class UserItem extends Component {

    handleEditUser = () => {
        console.log("edit");
        this.props.dispatch({
            type: "EDIT_USER",
            payload: this.props.user,
        });
    };

    render() {
        const {manv, tennv, email} = this.props.user;
        return (
            <tr>
                <td>{manv}</td>
                <td>{tennv}</td>
                <td>{email}</td>
                <td><button onClick={this.handleEditUser} className="btn btn-primary">Edit</button></td>
            </tr>
        )
    }
}

export default connect()(UserItem);
